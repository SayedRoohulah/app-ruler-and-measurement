package com.woheller69.level;

import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.woheller69.level.orientation.Orientation;
import com.woheller69.level.orientation.OrientationListener;
import com.woheller69.level.orientation.OrientationProvider;
import com.woheller69.level.view.LevelView;

import org.woheller69.level.R;

public class Level extends AppCompatActivity implements OrientationListener {

    private static Level CONTEXT;

    private OrientationProvider provider;

    private LevelView view;


    public static Level getContext() {
        return CONTEXT;
    }

    public static OrientationProvider getProvider() {
        return getContext().provider;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        getSupportActionBar().hide();
         CONTEXT = this;
        view = findViewById(R.id.main_levelView);

    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("Level", "Level resumed");
        provider = OrientationProvider.getInstance();
          // orientation manager
        if (provider.isSupported()) {
            provider.startListening(this);
        } else {
            Toast.makeText(this, getText(R.string.not_supported), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (provider.isListening()) {
            provider.stopListening();
        }
    }


    @Override
    public void onOrientationChanged(Orientation orientation, float pitch, float roll, float balance) {

        view.onOrientationChanged(orientation, pitch, roll, balance);
    }

    @Override
    public void onCalibrationReset(boolean success) {
        Toast.makeText(this, success ?
                        R.string.calibrate_restored : R.string.calibrate_failed,
                Toast.LENGTH_LONG).show();
    }

    @Override
    public void onCalibrationSaved(boolean success) {
        Toast.makeText(this, success ?
                        R.string.calibrate_saved : R.string.calibrate_failed,
                Toast.LENGTH_LONG).show();
    }
}
