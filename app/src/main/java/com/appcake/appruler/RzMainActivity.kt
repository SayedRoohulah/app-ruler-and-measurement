package com.appcake.appruler

import android.Manifest
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.*
import android.hardware.*
import android.hardware.camera2.*
import android.os.Build
import android.os.Bundle
import android.util.Size
import android.view.MotionEvent
import android.view.Surface
import android.view.View
import android.view.View.OnTouchListener
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import com.appcake.appruler.admob.RzAdsHandling
import com.appcake.appruler.databinding.RzActivityMainBinding
import com.appcake.appruler.room.activity.RzRoomDetailActivity
import com.appcake.appruler.room.activity.RzSettingsActivity
import com.appcake.appruler.room.activity.RzTipsandTrickActivity
import com.appcake.appruler.room.data.RzDataRecord
import com.appcake.appruler.room.viewmodel.RzDataRecordViewModel
import com.google.android.gms.ads.*
import com.google.android.gms.ads.rewarded.RewardedAd
import com.google.android.gms.ads.rewarded.RewardedAdLoadCallback
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.pushwoosh.Pushwoosh
import com.woheller69.level.Level
import kotlinx.coroutines.*
import java.io.ByteArrayOutputStream
import java.text.SimpleDateFormat
import java.util.*
import kotlin.math.pow


class RzMainActivity : AppCompatActivity() , View.OnClickListener , SensorEventListener{
    private lateinit var am_binding: RzActivityMainBinding
    private lateinit var rzdatarecordViewModel: RzDataRecordViewModel

//    @SuppressWarnings("deprecation")
//    private var client: GoogleApiClient? = null
    private var mGoogleSignInClient: GoogleSignInClient? = null


    var mRewardedAd: RewardedAd? = null
    private var am_isView: Boolean = true
    private var am_isCameraOpen: Boolean = false
    private var am_isFlashOn: Boolean = false


    private var am_downAngle:Double = 0.0
    private var am_upAngle:Double  = 0.0
    private var am_anglewithGround:Double = 0.0
    private var am_humanlength=200

    private var am_isGround = true

    private var am_objectheightFromGround: Double? = null
    private var am_distanceFromObject: Double? = null
    private var am_length_of_object: Double? = null


    private var am_sensorManager: SensorManager? = null
    private var am_accelerateSensor: Sensor? = null
    private var am_magneticSensor: Sensor? = null

    private var am_Geomagnetic: FloatArray?=null
    private lateinit var am_Gravity: FloatArray
    private var am_RR = FloatArray(9)
    private var am_Orientation = FloatArray(3)
    private var am_Rolls: String = ""


    private lateinit var am_ImageDimension: Size

    private lateinit var cameraDevice: CameraDevice
    private lateinit var cameraCaptureSession: CameraCaptureSession
    private lateinit var previewRequestBuilder: CaptureRequest.Builder
    private lateinit var previewRequest: CaptureRequest

    private var am_FX = 0f
    private var am_FY: Float = 0f
    private var am_pixelRatio: Float = 0f
    private var am_refLen: Float = 0f
    private var am_circlesH = 0
    private var am_circlesW: Int = 0
    private var am_small_localPosX: Int = 0
    private var am_small_localPosY: Int = 0

    private var am_threSam_hold: Int = 50

    private var am_Bmp: Bitmap? = null
    private lateinit var am_bitmap: Bitmap
    private var am_mCanvas: Canvas? = null
    private var am_inputUnit: String? = null
    private var outUnit: String? = "cm"
    private var am_mRefrence = false
    private var am_hold: Boolean? = null
    private var am_hold2: Boolean? = null
    private var am_mode: Boolean? = true
    private var am_accerateCameraMeasure:Boolean? = false
    private val am_distanceList = mutableListOf<Float>()
    private var am_distanceString: String? = null


    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        am_binding = RzActivityMainBinding.inflate(layoutInflater)
        setContentView(am_binding.root)

        supportActionBar?.hide()

        amInitializeVariables()

        ////////pushwoosh register////////
        Pushwoosh.getInstance().registerForPushNotifications()

        RzAdsHandling.init(this)


        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestEmail()
            .build()


        rzdatarecordViewModel = ViewModelProvider(this)[RzDataRecordViewModel::class.java]

//        dm = DisplayMetrics()
//        am_binding.crosshair.layoutParams.width = (dm.widthPixels * 15 / 100)
//        am_binding.crosshair.layoutParams.height = (dm.widthPixels * 15 / 100)

        am_binding.crosshair.setOnClickListener { am_binding.or.visibility = View.VISIBLE
            amTakeAngles() }
        am_binding.measureFloor.setOnClickListener {
            am_isGround = true
        }

        am_binding.previewView.setOnTouchListener(OnTouchListener { _, event ->
            amUpdateBulletsDimensions(event)
            amCheckEitherBulletwillMove()
            amResetBulletsProperties_whenActionUp(event)
            true
        })

        am_binding.ivFlashLight.setOnClickListener {
            try {
                if (am_isCameraOpen){
                    amToggleFlash()
                }else{
                    if (am_isFlashOn){
                        amFlashlightOn()
                    }else{
                        amFlashlightOff()
                    }
                    am_isFlashOn = !am_isFlashOn
                }
             } catch (e: Exception) {
                 e.printStackTrace()
             }

         }
        am_binding.ivRefresh.setOnClickListener  { amShowResetDialog() }
        am_binding.ivReset.setOnClickListener { amShowResetDialog() }

        am_binding.ivSetting.setOnClickListener {
            val intent = Intent(this,RzSettingsActivity::class.java)
            startActivity(intent)
        }
        am_binding.ivTip.setOnClickListener {
            val intent = Intent(this,RzTipsandTrickActivity::class.java)
            startActivity(intent)
        }
        am_binding.ivContinueLine.setOnClickListener {
            am_binding.imvCircle1.visibility = View.VISIBLE
            amKeepingMeasuredLine()
          }
        am_binding.ivPutline.setOnClickListener {
            amCheckCaseUserUse()
        }

        am_sensorManager = getSystemService(SENSOR_SERVICE) as SensorManager
        am_accelerateSensor = am_sensorManager!!.getDefaultSensor(Sensor.TYPE_ACCELEROMETER)
        am_magneticSensor = am_sensorManager!!.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD)
        am_sensorManager!!.registerListener(this, am_accelerateSensor, SensorManager.SENSOR_DELAY_NORMAL)
        am_sensorManager!!.registerListener(this, am_magneticSensor, SensorManager.SENSOR_DELAY_NORMAL)

        am_binding.ivShoot.setOnClickListener {
            amSaveData()
        }

//            val bitmap = Bitmap.createBitmap(am_binding.imageView.width, am_binding.imageView.height, Bitmap.Config.ARGB_8888)
//            val canvas = Canvas(bitmap)
//            am_binding.previewView.draw(canvas)




        am_binding.ivMore.setOnClickListener {
            if (am_isView) {
                am_binding.adView.visibility = View.GONE
                amAlertRewarded()
            //    Blurry.with(this).radius(25).sampling(2).onto(am_binding.mainLayout)

                am_binding.tapLayout.visibility = View.VISIBLE
                am_binding.camView.setOnClickListener(this)
                am_binding.measureVertical.setOnClickListener(this)
                am_binding.measureArea.setOnClickListener(this)
                am_binding.measureFloor.setOnClickListener(this)
                am_binding.waterLevel.setOnClickListener(this)

            }else{
                am_binding.tapLayout.visibility = View.GONE
                am_binding.adView.visibility = View.VISIBLE

            }
            am_isView = !am_isView
        }

        // client = GoogleApiClient.Builder(this).addApi(AppIndex.API).build()
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso)



    }
     private fun amSaveData() {
        try {
            am_bitmap = amGetScreenshotFromView(am_binding.imageView)
            //  val bitmap = am_binding.previewView.getBitmap()
            val amCalender = Calendar.getInstance().time

            val amdateFormat = SimpleDateFormat("dd-MMM-yyyy", Locale.getDefault())
            val amformattedDate: String = amdateFormat.format(amCalender)
            val amId = 0L
            val amName = "Room"
            val amDate = amformattedDate
            if (am_accerateCameraMeasure == true){
                val amMeasurement = am_binding.or.text.toString()

                val amItem = RzDataRecord(
                    id = amId,
                    name = amName,
                    image = am_bitmap,
                    measure = amMeasurement,
                    date = amDate
                )
                rzdatarecordViewModel.insert(amItem)
                amShowSaveDialog(amItem)
            }else{
                val amItem = RzDataRecord(
                    id = amId,
                    name = amName,
                    image = am_bitmap,
                    measure = am_distanceString!!,
                    date = amDate
                )
                rzdatarecordViewModel.insert(amItem)
                amShowSaveDialog(amItem)
            }


        }catch (e:Exception){
            e.printStackTrace()
        }
    }

    private fun amInitializeVariables() {
        RzAdsHandling.bannerAd(this,findViewById(R.id.ad_view))
        am_hold2 = false
        am_hold = am_hold2
        am_binding.imvCircle1.alpha = 0.3f
        am_binding.imvCircle2.alpha = 0.3f
        am_hold2 = false
        am_hold = am_hold2
        am_binding.imvCircle2.visibility = View.INVISIBLE
        am_binding.imvCircle1.visibility = View.INVISIBLE
    }

    private fun amCameraIsOpen(){
        am_binding.previewView.visibility = View.VISIBLE
        am_binding.adView.visibility = View.GONE

        amOpenCamera()
            am_isCameraOpen = true

    }
    private fun amReset() {
        am_binding.or.text = ""
        am_binding.or.visibility = View.INVISIBLE
        am_binding.imvCircle1.visibility = View.INVISIBLE
        am_binding.imvCircle2.visibility = View.INVISIBLE
        am_downAngle = 0.0
        am_upAngle = 0.0
        am_anglewithGround = 0.0
        am_objectheightFromGround = 0.0
        am_length_of_object = 0.0
        am_distanceFromObject = 0.0
        am_binding.imgTmp.setImageBitmap(null)
        am_binding.imageView.setImageBitmap(null)
        am_binding.imgTmp.invalidate()
        am_binding.imageView.invalidate()
        am_distanceList.clear()
        am_distanceString = ""
        am_binding.imageView.setImageResource(0)

    }


     @RequiresApi(Build.VERSION_CODES.M)
    private fun amFlashlightOff() {
         am_binding.light.setImageResource(R.drawable.flashlightoff)
         val cameraManager: CameraManager = getSystemService(Context.CAMERA_SERVICE) as CameraManager
        val amCameraId = cameraManager.cameraIdList[0]
        cameraManager.setTorchMode(amCameraId, false)
    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun amFlashlightOn() {
        am_binding.light.setImageResource(R.drawable.flashlighton)
        val cameraManager: CameraManager = getSystemService(Context.CAMERA_SERVICE) as CameraManager
        val amCameraId = cameraManager.cameraIdList[0]
        cameraManager.setTorchMode(amCameraId, true)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.camView -> {
                am_binding.waterLevel.setImageResource(R.drawable.water_level_gray)
                am_binding.measureFloor.setImageResource(R.drawable.ms_ground_gray)
                am_binding.measureArea.setImageResource(R.drawable.area_gray)
                am_binding.measureVertical.setImageResource(R.drawable.ms_vertical_gray)
                am_binding.camView.setImageResource(R.drawable.cam_color)
                am_binding.changeVector.setImageResource(R.drawable.cam_color)
                amCameraIsOpen()
                am_accerateCameraMeasure = true
                am_binding.crosshair.visibility = View.VISIBLE
                am_binding.imvCircle1.visibility = View.INVISIBLE
                am_binding.imvCircle2.visibility = View.INVISIBLE
                am_binding.imageView.visibility = View.INVISIBLE
                am_binding.imgTmp.visibility = View.INVISIBLE

             }
            R.id.measureVertical -> {
                am_binding.camView.setImageResource(R.drawable.cam_gray)
                am_binding.waterLevel.setImageResource(R.drawable.water_level_gray)
                am_binding.measureFloor.setImageResource(R.drawable.ms_ground_gray)
                am_binding.measureArea.setImageResource(R.drawable.area_gray)
                am_binding.measureVertical.setImageResource(R.drawable.ms_vertical_color)
                am_binding.changeVector.setImageResource(R.drawable.ms_vertical_color)
                am_isGround = false
                am_accerateCameraMeasure = false

                amCameraIsOpen()

                am_binding.crosshair.visibility = View.INVISIBLE
                am_binding.imageView.visibility = View.VISIBLE
                am_binding.imgTmp.visibility = View.VISIBLE
                am_binding.imvCircle2.alpha = 0.3f
                am_binding.imvCircle2.visibility = View.VISIBLE
                am_binding.imvCircle1.visibility = View.VISIBLE

                //  ref_len = Float.parseFloat(editText.getText().toString());
                am_refLen = 260f
                am_mode = false

            }
            R.id.measureArea -> {
                am_binding.camView.setImageResource(R.drawable.cam_gray)
                am_binding.measureVertical.setImageResource(R.drawable.ms_vertical_gray)
                am_binding.waterLevel.setImageResource(R.drawable.water_level_gray)
                am_binding.measureFloor.setImageResource(R.drawable.ms_ground_gray)
                am_binding.measureArea.setImageResource(R.drawable.area_color)
                am_binding.changeVector.setImageResource(R.drawable.area_color)
                am_accerateCameraMeasure = false

                amCameraIsOpen()

                am_binding.crosshair.visibility = View.INVISIBLE
                am_binding.imageView.visibility = View.VISIBLE
                am_binding.imgTmp.visibility = View.VISIBLE
                am_binding.imvCircle2.visibility = View.VISIBLE
                am_binding.imvCircle1.visibility = View.VISIBLE
                am_refLen = 36.37f //diameter


            }
            R.id.measureFloor -> {
                 am_binding.camView.setImageResource(R.drawable.cam_gray)
                am_binding.measureVertical.setImageResource(R.drawable.ms_vertical_gray)
                am_binding.measureArea.setImageResource(R.drawable.area_gray)
                am_binding.waterLevel.setImageResource(R.drawable.water_level_gray)
                am_binding.measureFloor.setImageResource(R.drawable.ms_ground_color)
                am_binding.changeVector.setImageResource(R.drawable.ms_ground_color)
                am_isGround = true
                am_accerateCameraMeasure = false

                amCameraIsOpen()

                am_binding.crosshair.visibility = View.INVISIBLE
                am_binding.imageView.visibility = View.VISIBLE
                am_binding.imgTmp.visibility = View.VISIBLE
                am_binding.imvCircle2.visibility = View.VISIBLE
                am_binding.imvCircle1.visibility = View.VISIBLE
                am_binding.imvCircle2.alpha = 0.3f

                //ref_len = 3.14f
                am_refLen = 365.76f
                am_mode = false

            }
            R.id.waterLevel -> {
                am_binding.camView.setImageResource(R.drawable.cam_gray)
                am_binding.measureVertical.setImageResource(R.drawable.ms_vertical_gray)
                am_binding.measureArea.setImageResource(R.drawable.area_gray)
                am_binding.measureFloor.setImageResource(R.drawable.ms_ground_gray)
                am_binding.waterLevel.setImageResource(R.drawable.water_level_color)
                am_binding.changeVector.setImageResource(R.drawable.water_level_color)

                val intent = Intent(this, Level::class.java)
                startActivity(intent)

            }

        }
    }

    private val am_cameraDeviceStateCallback = object : CameraDevice.StateCallback() {
        override fun onOpened(camera: CameraDevice) {
            cameraDevice = camera
            amCreateCameraPreviewSession()
        }

        override fun onDisconnected(camera: CameraDevice) {
            cameraDevice.close()
        }

        override fun onError(camera: CameraDevice, error: Int) {
            cameraDevice.close()
          }
    }

    private fun amCreateCameraPreviewSession() {
        val am_surfaceTexture = am_binding.previewView.surfaceTexture
        am_surfaceTexture?.setDefaultBufferSize(1280,720)
        val surface = Surface(am_surfaceTexture)
        previewRequestBuilder = cameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW)
        previewRequestBuilder.addTarget(surface)
        if (am_isFlashOn) {
            previewRequestBuilder.set(CaptureRequest.FLASH_MODE, CaptureRequest.FLASH_MODE_TORCH)
        }
        cameraDevice.createCaptureSession(listOf(surface),
            object : CameraCaptureSession.StateCallback() {

                override fun onConfigured(session: CameraCaptureSession) {
                    cameraCaptureSession = session
                    previewRequestBuilder.set(CaptureRequest.CONTROL_AF_MODE, CaptureRequest.CONTROL_AF_MODE_CONTINUOUS_PICTURE)
                    previewRequest = previewRequestBuilder.build()
                    cameraCaptureSession.setRepeatingRequest(previewRequest, null, null)
                }

                override fun onConfigureFailed(session: CameraCaptureSession) {
                    Toast.makeText(this@RzMainActivity, "Failed", Toast.LENGTH_SHORT).show()
                }
            }, null)
    }


    private fun amOpenCamera() {
        val manager = getSystemService(Context.CAMERA_SERVICE) as CameraManager
        try {
            val cameraId = manager.cameraIdList[0]
            val characteristics = manager.getCameraCharacteristics(cameraId)
            val map = characteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP)
            am_ImageDimension = map?.getOutputSizes(SurfaceTexture::class.java)?.get(0) ?: Size(640, 480)

            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.CAMERA), 100)
                return
            }
            manager.openCamera(cameraId, am_cameraDeviceStateCallback, null)
        } catch (e: CameraAccessException) {
            e.printStackTrace()
        }
    }


    private fun amToggleFlash() {
        if (am_isFlashOn) {
            am_binding.light.setImageResource(R.drawable.flashlighton)
            previewRequestBuilder.set(CaptureRequest.FLASH_MODE, CaptureRequest.FLASH_MODE_TORCH)
        } else {
            am_binding.light.setImageResource(R.drawable.flashlightoff)

            previewRequestBuilder.set(CaptureRequest.FLASH_MODE, CaptureRequest.FLASH_MODE_OFF)
        }
        am_isFlashOn = !am_isFlashOn

        cameraCaptureSession.setRepeatingRequest(previewRequestBuilder.build(), null, null)
    }

    override fun onResume() {
        super.onResume()
        am_sensorManager?.registerListener(this, am_accelerateSensor, SensorManager.SENSOR_DELAY_NORMAL)
        am_sensorManager?.registerListener(this, am_magneticSensor, SensorManager.SENSOR_DELAY_NORMAL)
    }

    override fun onPause() {
        super.onPause()
        am_sensorManager?.unregisterListener(this, am_accelerateSensor)
        am_sensorManager?.unregisterListener(this, am_magneticSensor)
    }
    override fun onSensorChanged(event: SensorEvent?) {
        if (event?.sensor?.getType() == Sensor.TYPE_MAGNETIC_FIELD)
            am_Geomagnetic = event.values.clone()
        else if (event?.sensor?.getType() == Sensor.TYPE_ACCELEROMETER)
            am_Gravity = event.values.clone()
         if (am_Geomagnetic != null) {
            SensorManager.getRotationMatrix(am_RR, null, am_Gravity, am_Geomagnetic)
            SensorManager.getOrientation(am_RR, am_Orientation)
         }

    }

    override fun onAccuracyChanged(sensor: Sensor?, accuracy: Int) {
     }

      private fun amBaseCase() {
        am_downAngle = Math.abs(am_downAngle)
        am_upAngle = Math.abs(am_upAngle)
        am_distanceFromObject = am_humanlength / Math.tan(Math.toRadians(am_downAngle))
        am_length_of_object =
            am_humanlength + Math.tan(Math.toRadians(am_upAngle)) * am_distanceFromObject!!
        if (am_length_of_object!! / 100 > 0) {
            am_binding.or.setText(
                "am_length_of_object :\n" + ("""${
                    String.format(
                        "%.2f",
                        am_length_of_object!! / 100
                    )
                } M distance_from_object : """ + String.format(
                    "%.2f",
                    am_distanceFromObject!! / 100
                ) + " M")
            )
            am_binding.or.setVisibility(View.VISIBLE)
        } else {
            Toast.makeText(this, "Move Forward", Toast.LENGTH_LONG).show()
            am_downAngle = 0.0
            am_upAngle = 0.0
        }
    }

    /**
     * I-measure object started from ground and shorter than human.
     */
    private fun amMeasureSmallObject() {
        am_downAngle = Math.abs(am_downAngle)
        am_upAngle = Math.abs(am_upAngle)
        val distance_angle = 90 - am_downAngle
        am_distanceFromObject = am_humanlength * Math.tan(Math.toRadians(distance_angle))
        val part_of_my_tall = am_distanceFromObject!! * Math.tan(Math.toRadians(am_upAngle))
        am_length_of_object = am_humanlength - part_of_my_tall
        if (am_length_of_object!! / 100 > 0) {
            am_binding.or.text =
                "am_length_of_object :\n" + ("""${
                String.format(
                    "%.2f",
                    am_length_of_object!! / 100
                )
            } M distance_from_object : """ + String.format(
                "%.2f",
                am_distanceFromObject!! / 100
            ) + " M")
            am_binding.or.setVisibility(View.VISIBLE)
        } else {
            Toast.makeText(this, "Move Forward", Toast.LENGTH_LONG).show()
            am_downAngle = 0.0
            am_upAngle = 0.0
        }
    }

    /**
     * I-take angles from sensors.
     */
    private fun amTakeAngles() {
        am_Rolls += """ ${Math.toDegrees(am_Orientation[2].toDouble()) % 360 + 90} """.trimIndent()
        if (!am_isGround && am_anglewithGround == 0.0)
            am_anglewithGround = amAdjustAngleRotation(Math.toDegrees(am_Orientation[2].toDouble()) % 360 + 90)
        else if (am_downAngle == 0.0)
            am_downAngle = amAdjustAngleRotation(Math.toDegrees(am_Orientation[2].toDouble()) % 360 + 90)
        else if (am_upAngle == 0.0) {
            am_upAngle = amAdjustAngleRotation(Math.toDegrees(am_Orientation[2].toDouble()) % 360 + 90)
            if (!am_isGround)
                amObjectCalculationsDoesntTouchGround()
            else
                amObjectCalculationsTouchGround()
        }
    }


    /**
     * I-adjust angle rotation.
     */
    private fun amAdjustAngleRotation(angle: Double): Double {
        var temp: Double
        temp = angle
        if (temp > 90) {
            temp = 180 - temp
        }
        return temp
    }



      private fun amObjectCalculationsTouchGround() {
        if (am_downAngle < 0 && am_upAngle > 0) //base case
        {
            val temp = am_upAngle
            am_upAngle = am_downAngle
            am_downAngle = temp
            amBaseCase()
        } else if (am_downAngle > 0 && am_upAngle > 0 && am_downAngle < am_upAngle) //smaller object
        {
            val temp = am_upAngle
            am_upAngle = am_downAngle
            am_downAngle = temp
            amMeasureSmallObject()
        } else if (am_upAngle < 0 && am_downAngle > 0) //base case
            amBaseCase() else  //smaller object
            amMeasureSmallObject()
    }


      private fun amObjectCalculationsDoesntTouchGround() {
        if (am_anglewithGround > 0 && am_downAngle > 0 && am_upAngle < 0)
            amObjectOnEyesLevelCalc()
        else if (am_anglewithGround > 0 && am_downAngle < 0 && am_upAngle < 0)
            amObjectUpperEyesLevelCalc()
        else if (am_anglewithGround > 0 && am_downAngle > 0 && am_upAngle > 0)
            amObjectBelowEyesLevelCalc()
    }


      private fun amObjectOnEyesLevelCalc() {
        am_downAngle = Math.abs(am_downAngle)
        am_upAngle = Math.abs(am_upAngle)
        am_anglewithGround = 90 - am_anglewithGround
        am_distanceFromObject = am_humanlength * Math.tan(Math.toRadians(am_anglewithGround))
        val partDown = am_distanceFromObject!! * Math.tan(Math.toRadians(am_downAngle))
        val partUp = am_distanceFromObject!! * Math.tan(Math.toRadians(am_upAngle))
        am_length_of_object = partDown + partUp
        am_objectheightFromGround = am_humanlength - partDown
        am_binding.or.setText(
            """am_length_of_object :
        ${String.format("%.2f", am_length_of_object!! / 100)} M
        distance_from_object : """ +
                    String.format("%.2f", am_distanceFromObject!! / 100) +
                    " M" + "\n" + "height_from_ground :\n" +
                    String.format("%.2f", am_objectheightFromGround!! / 100) + " M"
        )
        am_binding.or.setVisibility(View.VISIBLE)
    }

      private fun amObjectUpperEyesLevelCalc() {
        am_downAngle = Math.abs(am_downAngle)
        am_upAngle = Math.abs(am_upAngle)
        am_anglewithGround = 90 - am_anglewithGround
        am_distanceFromObject = am_humanlength * Math.tan(Math.toRadians(am_anglewithGround))
        val part = am_distanceFromObject!! * Math.tan(Math.toRadians(am_downAngle))
        val all = am_distanceFromObject!! * Math.tan(Math.toRadians(am_downAngle))
        am_length_of_object = all - part
        am_objectheightFromGround = am_humanlength + part
          am_binding.or.text = """am_length_of_object :
                  ${String.format("%.2f", am_length_of_object!! / 100)} M
                  distance_from_object :
                  ${String.format("%.2f", am_distanceFromObject!! / 100)} M
                  height_from_ground :
                  ${String.format("%.2f", am_objectheightFromGround!! / 100)} M"""
        am_binding.or.setVisibility(View.VISIBLE)
    }


    private fun amObjectBelowEyesLevelCalc() {
        am_downAngle = Math.abs(am_downAngle)
        am_upAngle = Math.abs(am_upAngle)
        am_anglewithGround = 90 - am_anglewithGround
        am_distanceFromObject = am_humanlength * Math.tan(Math.toRadians(am_anglewithGround))
        val all = am_distanceFromObject!! * Math.tan(Math.toRadians(am_downAngle))
        val part = am_distanceFromObject!! * Math.tan(Math.toRadians(am_upAngle))
        am_length_of_object = all - part
        am_objectheightFromGround = am_humanlength - all
        am_binding.or.setText(
            """am_length_of_object :
            ${String.format("%.2f", am_length_of_object!! / 100)} M
            distance_from_object :
            ${String.format("%.2f", am_distanceFromObject!! / 100)} M
            height_from_ground :
            ${String.format("%.2f", am_objectheightFromGround!! / 100)} M"""
        )
        am_binding.or.visibility = View.VISIBLE
    }


    private fun amResetBulletsProperties_whenActionUp(event: MotionEvent) {
        if (event.action == MotionEvent.ACTION_UP) {
            am_binding.imvCircle1.alpha = 0.3f
            am_binding.imvCircle2.alpha = 0.3f
            am_hold = false
            am_hold2 = false
        }
    }


    private fun amUpdateBulletsDimensions(event: MotionEvent) {
        am_FX = event.x
        am_FY = event.y
        am_circlesW = am_binding.imvCircle1.width
        am_circlesH = am_binding.imvCircle1.height
    }


    private fun amCheckEitherBulletwillMove() {
        if (amMoveCircles(am_FX, am_FY) == 1 || am_hold == true) {
            am_hold = true
            am_binding.imvCircle1.x = am_FX - (am_circlesW / 2)
            am_binding.imvCircle1.y = am_FY - (am_circlesH / 2)
            am_binding.imvCircle1.alpha = 0.8f
            am_binding.imvCircle2.alpha = 0.3f
            if (!am_mode!!) amCrearPunto(
                am_binding.imvCircle1.x + am_circlesW / 2,
                am_binding.imvCircle1.y + am_circlesH / 2,
                am_binding.imvCircle2.x + am_circlesW / 2,
                am_binding.imvCircle2.y + am_circlesH / 2,
                Color.YELLOW
            )
        } else if (amMoveCircles(am_FX, am_FY) == 2 || am_hold2 == true) {
            am_hold2 = true
            am_binding.imvCircle2.x = am_FX - (am_circlesW / 2)
            am_binding.imvCircle2.y = am_FY - (am_circlesH / 2)
            am_binding.imvCircle1.alpha = 0.3f
            am_binding.imvCircle2.alpha = 0.8f
            amCrearPunto(
                am_binding.imvCircle1.x + am_circlesW / 2,
                am_binding.imvCircle1.y + am_circlesH / 2,
                am_binding.imvCircle2.x + am_circlesW / 2,
                am_binding.imvCircle2.y + am_circlesH / 2,
                Color.YELLOW
            )
        }
    }


    private fun amCheckCaseUserUse() {
        try {
            //custom case.
            if (!am_mode!!) {
                val corRefDistance = Math.sqrt(
                    (am_binding.imvCircle1.x - am_binding.imvCircle2.x).toDouble().pow(2.0)
                            + (am_binding.imvCircle1.y - am_binding.imvCircle2.y).toDouble().pow(2.0)
                ).toFloat()
                amGetPixelRatio(corRefDistance)
            } else if ((am_mode)!!) {
                am_Bmp = Bitmap.createBitmap(
                    am_binding.imageView.width,
                    am_binding.imageView.height,
                    Bitmap.Config.ARGB_8888
                )
                am_mCanvas = Canvas(am_Bmp!!)
                am_binding.imgTmp.draw(am_mCanvas)
                val am_corRefDistance = amRegionGrowing(am_FX.toInt(), am_FY.toInt(), am_threSam_hold).toFloat()
                amGetPixelRatio(am_corRefDistance)
                am_binding.imageView.setImageBitmap(am_Bmp)
                am_binding.imvCircle2.visibility = View.VISIBLE
                am_mode = false
            }
            amKeepingMeasuredLine()
            am_mRefrence = true
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
    }


    private fun amGetPixelRatio(CoorRefDistance: Float) {
        am_pixelRatio = am_refLen / CoorRefDistance
    }



    private fun amMoveCircles(x_t: Float, y_t: Float): Int {
        am_small_localPosX = am_binding.imvCircle1.x.toInt()
        am_small_localPosY = am_binding.imvCircle1.y.toInt()
        if (x_t > am_small_localPosX && x_t < (am_small_localPosX + am_circlesW)) if (y_t > am_small_localPosY && y_t < (am_small_localPosY + am_circlesH)) return 1
        am_small_localPosX = am_binding.imvCircle2.x.toInt()
        am_small_localPosY = am_binding.imvCircle2.y.toInt()
        if (x_t > am_small_localPosX && x_t < (am_small_localPosX + am_circlesW)) if (y_t > am_small_localPosY && y_t < (am_small_localPosY + am_circlesH)) return 2
        return 0
    }


    private fun amCrearPunto(x: Float, y: Float, xend: Float, yend: Float, color: Int) {
        var actual_dist: Float? = null
        am_Bmp = Bitmap.createBitmap(
            am_binding.imageView.width,
            am_binding.imageView.height,
            Bitmap.Config.ARGB_8888
        )

        am_mCanvas = Canvas(am_Bmp!!)
        am_binding.imgTmp.draw(am_mCanvas)
        val p = Paint()
        val p1 = Paint()
        p1.strokeWidth = 5f
        p1.color = Color.YELLOW
        p.strokeWidth = 5f
        p.color = color
        am_mCanvas!!.drawCircle(x, y, 10f, p1)
        am_mCanvas!!.drawCircle(xend, yend, 10f, p1)
        am_mCanvas!!.drawLine(x, y, xend, yend, p)
        if (am_mRefrence) {
              actual_dist = Math.sqrt(
                ((this.am_binding.imvCircle1.x - am_binding.imvCircle2.x).toDouble().pow(2.0)
                        + (am_binding.imvCircle1.y - am_binding.imvCircle2.y).toDouble().pow(2.0))
            ).toFloat()
            actual_dist *= (am_pixelRatio)
            if (am_inputUnit === "cm" && outUnit === "in")
                actual_dist *= 0.393701.toFloat()
            else if (am_inputUnit === "in" && outUnit === "cm")
                actual_dist /= 0.393701.toFloat()

            val text_paint = Paint()
            text_paint.color = Color.RED
            text_paint.style = Paint.Style.FILL_AND_STROKE
            text_paint.textSize = 30f
            text_paint.strokeWidth = 3f
            val text_x = Math.abs((am_binding.imvCircle1.x - am_binding.imvCircle2.x) / 2) + Math.min(
                am_binding.imvCircle1.x,
                am_binding.imvCircle2.x
            )
            val text_y = Math.abs((am_binding.imvCircle1.y - am_binding.imvCircle2.y) / 2) + Math.min(
                am_binding.imvCircle1.y,
                am_binding.imvCircle2.y
            )
            am_mCanvas!!.drawText(
                (String.format("%.2f", actual_dist) + " " + outUnit),
                text_x,
                text_y,
                text_paint
            )
//            val am_distanceString = String.format("%.2f", actual_dist) + " " + outUnit

        }
        am_distanceList.add(actual_dist!!)
        am_distanceString = am_distanceList.joinToString(", ") { String.format("%.2f $outUnit", it) }

        am_binding.imageView.setImageBitmap(am_Bmp)

    }


    private fun amKeepingMeasuredLine() {
        am_binding.imvCircle1.x = (am_binding.imageView.width / 2).toFloat()
        am_binding.imvCircle1.setY(45f)
        am_binding.imvCircle2.x = (am_binding.imageView.width / 2).toFloat()
        am_binding.imvCircle2.y = (am_binding.imageView.height / 2).toFloat()
        am_binding.imgTmp.setImageBitmap(am_Bmp)
    }


    private fun amRegionGrowing(x: Int, y: Int, am_threSam_hold: Int): Double {
        var x = x
        var y = y
        val open = TreeSet<Int>()
        val closed = TreeSet<Int>()
         //scale image and x,y
        val image = amScaleDown((am_Bmp!!), 800f, true)
        x = (x * image.width.toFloat() / am_Bmp!!.width.toFloat()).toInt()
        y = (y * image.height.toFloat() / am_Bmp!!.height.toFloat()).toInt()
        //get Seed Node RGB values
        val color = image.getPixel(x, y)
        val am_A = Color.alpha(color)
        val am_R = Color.red(color)
        val am_G = Color.green(color)
        val am_B = Color.blue(color)
        var am_xMin = x
        var am_xMax = x
        var am_yMin = y
        var am_yMax = y
        var am_yMin2 = y
        var am_yMax2 = y
        val am_seedNode = x * image.width + y
        open.add(am_seedNode)
        while (!open.isEmpty()) {
            // remove current node from open list and add to closed list
            val currentNode = open.first()
            closed.add(currentNode)
            open.remove(currentNode)
            //get current node coordinates
            val xCurr = currentNode / image.width
            val yCurr = currentNode % image.width
            var upColor = 0
            var downColor = 0
            var leftColor = 0
            var rightColor = 0
            if (xCurr - 1 > 0) upColor = image.getPixel(xCurr - 1, yCurr)
            if (xCurr + 1 < image.width) downColor = image.getPixel(xCurr + 1, yCurr)
            if (yCurr - 1 > 0) leftColor = image.getPixel(xCurr, yCurr - 1)
            if (yCurr + 1 < image.height) rightColor = image.getPixel(xCurr, yCurr + 1)
            //Check upper Pixel
            if (((xCurr - 1 > 0) && (Math.abs(am_R - Color.red((upColor))) <= am_threSam_hold
                        ) && (Math.abs(am_B - Color.blue((upColor))) <= am_threSam_hold) && (Math.abs(
                    am_G - Color.green(
                        (upColor)
                    )
                ) <= am_threSam_hold
                        ) && (Math.abs(am_A - Color.alpha(upColor)) <= am_threSam_hold) &&
                        !closed.contains(currentNode - image.width) && !open.contains(currentNode - image.width))
            ) open.add((xCurr - 1) * image.width + yCurr)
            //Check lower Pixel
            if ((xCurr + 1 < image.width) && (Math.abs(am_R - Color.red((downColor))) <= am_threSam_hold
                        ) && (Math.abs(am_B - Color.blue((downColor))) <= am_threSam_hold
                        ) && (Math.abs(am_G - Color.green((downColor))) <= am_threSam_hold
                        ) && (Math.abs(am_A - Color.alpha(downColor)) <= am_threSam_hold
                        ) && !closed.contains(currentNode + image.width) && !open.contains(
                    currentNode + image.width
                )
            ) open.add((xCurr + 1) * image.width + yCurr)
            //Check left Pixel
            if (((yCurr - 1 > 0) && (Math.abs(am_R - Color.red((leftColor))) <= am_threSam_hold
                        ) && (Math.abs(am_B - Color.blue((leftColor))) <= am_threSam_hold) && (Math.abs(
                    am_G - Color.green(
                        (leftColor)
                    )
                ) <= am_threSam_hold
                        ) && (Math.abs(am_A - Color.alpha(leftColor)) <= am_threSam_hold) && !closed.contains(
                    currentNode - 1
                )
                        && !open.contains(currentNode - 1))
            ) open.add((xCurr) * image.width + yCurr - 1)
            //Check right Pixel
            if (((yCurr + 1 < image.height) && (Math.abs(am_R - Color.red((rightColor))) <= am_threSam_hold
                        ) && (Math.abs(am_B - Color.blue((rightColor))) <= am_threSam_hold
                        ) && (Math.abs(am_G - Color.green((rightColor))) <= am_threSam_hold
                        ) && (Math.abs(am_A - Color.alpha(rightColor)) <= am_threSam_hold) && !closed.contains(
                    currentNode + 1
                )
                        && !open.contains(currentNode + 1))
            ) open.add(((xCurr) * image.width) + yCurr + 1)
            image.setPixel(xCurr, yCurr, 0)
            //check for min & max x,y
            if (xCurr < am_xMin) {
                am_xMin = xCurr
                am_yMin2 = yCurr
                am_yMin = am_yMin2
            } else if (xCurr == am_xMin) {
                am_yMin = Math.min(am_yMin, yCurr)
                am_yMin2 = Math.max(am_yMin2, yCurr)
            }
            if (xCurr > am_xMax) {
                am_xMax = xCurr
                am_yMax2 = yCurr
                am_yMax = am_yMax2
            } else if (xCurr == am_xMax) {
                am_yMax = Math.max(am_yMax, yCurr)
                am_yMax2 = Math.min(am_yMax2, yCurr)
            }
        }
        image.setPixel(am_xMin, am_yMin, Color.BLUE)
        image.setPixel(am_xMax, am_yMax, Color.BLUE)
        image.setPixel(am_xMin, am_yMin2, Color.GREEN)
        image.setPixel(am_xMax, am_yMax2, Color.GREEN)
        am_xMax = (am_xMax * (am_Bmp!!.width.toFloat() / image.width.toFloat())).toInt()
        am_xMin = (am_xMin * (am_Bmp!!.width.toFloat() / image.width.toFloat())).toInt()
        am_yMax = (am_yMax * (am_Bmp!!.height.toFloat() / image.height.toFloat())).toInt()
        am_yMin = (am_yMin * (am_Bmp!!.height.toFloat() / image.height.toFloat())).toInt()
        am_yMax2 = (am_yMax2 * (am_Bmp!!.height.toFloat() / image.height.toFloat())).toInt()
        am_yMin2 = (am_yMin2 * (am_Bmp!!.height.toFloat() / image.height.toFloat())).toInt()
        val dist1 = Math.sqrt(
            (am_xMax - am_xMin).toDouble().pow(2.0) + (am_yMax - am_yMin).toDouble().pow(2.0)
        )
        val dist2 = Math.sqrt(
            (am_xMax - am_xMin).toDouble().pow(2.0) + (am_yMax2 - am_yMin2).toDouble().pow(2.0)
        )
        am_Bmp = amScaleDown(image, Math.max(am_Bmp!!.width, am_Bmp!!.height).toFloat(), true)
        return Math.max(dist1, dist2)
    }

    private fun amScaleDown(realImage: Bitmap, maxImageSize: Float, filter: Boolean): Bitmap {
        val ratio = Math.min(
            maxImageSize / realImage.width,
            maxImageSize / realImage.height
        )
        val width = Math.round(ratio * realImage.width)
        val height = Math.round(ratio * realImage.height)
        return Bitmap.createScaledBitmap(
            realImage, width,
            height, filter
        )
    }

    private fun amShowResetDialog( ) {
        val am_dialog = Dialog(this)
        am_dialog.setCancelable(false)
        am_dialog.setContentView(R.layout.rz_custom_dialog)
        val yesBtn = am_dialog.findViewById(R.id.btn_Agree) as TextView
        val noBtn = am_dialog.findViewById(R.id.btn_Cancel) as TextView
        yesBtn.setOnClickListener {
            amReset()
            am_dialog.dismiss()
        }
        noBtn.setOnClickListener {
            am_dialog.dismiss()
        }
        am_dialog.show()

    }
    private fun amShowSaveDialog(item: RzDataRecord){
        val am_dialog = Dialog(this)
        am_dialog.setCancelable(false)
        am_dialog.setContentView(R.layout.rz_custom_dialog)
        val dialogTitle = am_dialog.findViewById(R.id.tvTitle) as TextView
        dialogTitle.text = "Plan saved to library"
        val dialogBody = am_dialog.findViewById(R.id.tvBody) as TextView
        dialogBody.text = "Tap open to edit/share the plan. Area: 45 sq.foots"

        val yesBtn = am_dialog.findViewById(R.id.btn_Agree) as TextView
        yesBtn.text = "OPEN"
        val noBtn = am_dialog.findViewById(R.id.btn_Cancel) as TextView
        noBtn.text = "CLOSE"
        yesBtn.setOnClickListener {
            val intent = Intent(this, RzRoomDetailActivity::class.java)
            val stream = ByteArrayOutputStream()
            am_bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream)
            val byteArray: ByteArray = stream.toByteArray()
            //intent.putExtra("name",current.name)
            intent.putExtra("id",item.id)
            intent.putExtra("image",byteArray)
            intent.putExtra("date",item.date)
            intent.putExtra("measure",item.measure)
            startActivity(intent)
            am_dialog.dismiss()
        }
        noBtn.setOnClickListener {
            am_dialog.dismiss()
        }
        am_dialog.show()
    }


    private fun amGetScreenshotFromView(view: View): Bitmap {
        val bitmap = Bitmap.createBitmap(view.width, view.height, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        view.draw(canvas)
        return bitmap
    }


    private fun amAlertRewarded() {
        lifecycleScope.launch {
            val am_progressDialog = Dialog(this@RzMainActivity)
            am_progressDialog.setContentView(R.layout.rz_custom_progress)

            am_progressDialog.setCancelable(false)
            am_progressDialog.show()
            amLoadRewardAd(am_progressDialog)
        }


    }

    private fun amLoadRewardAd(dialog: Dialog) {
        var adRequest = AdRequest.Builder().build()
        RewardedAd.load(this,getString(R.string.rewardedId), adRequest, object : RewardedAdLoadCallback() {
            override fun onAdFailedToLoad(adError: LoadAdError) {
                 mRewardedAd = null
                dialog.dismiss()

            }

            override fun onAdLoaded(ad: RewardedAd) {
                mRewardedAd = ad
                dialog.dismiss()
                amShowAd()

            }
        })
    }

    private fun amShowAd() {
        mRewardedAd?.let { ad ->
            ad.show(this, OnUserEarnedRewardListener { rewardItem ->
                // Handle the reward.
                val rewardAmount = rewardItem.amount
                val rewardType = rewardItem.type
                am_binding.tapLayout.visibility = View.VISIBLE

             })
        } ?: run {
            Toast.makeText(this, "The rewarded ad wasn't ready yet.", Toast.LENGTH_SHORT).show()
         }

        mRewardedAd!!.fullScreenContentCallback = object : FullScreenContentCallback() {
            override fun onAdShowedFullScreenContent() {}
            override fun onAdFailedToShowFullScreenContent(adError: AdError) {}
            override fun onAdDismissedFullScreenContent() {
                mRewardedAd = null
            }
        }
    }



}

