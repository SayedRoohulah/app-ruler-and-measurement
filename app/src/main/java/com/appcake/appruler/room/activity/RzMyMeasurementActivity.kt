package com.appcake.appruler.room.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.SearchView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.appcake.appruler.RzMainActivity
import com.appcake.appruler.R
import com.appcake.appruler.admob.RzAdsHandling
import com.appcake.appruler.databinding.RzActivityMyMeasurementBinding
import com.appcake.appruler.room.adapter.RzMyAdapter
import com.appcake.appruler.room.viewmodel.RzDataRecordViewModel
import java.util.Locale.filter

class RzMyMeasurementActivity : AppCompatActivity() {
    private lateinit var am_binding: RzActivityMyMeasurementBinding
    private lateinit var datarecordViewModel: RzDataRecordViewModel

    override fun onCreate(am_savedInstanceState: Bundle?) {
        super.onCreate(am_savedInstanceState)
        am_binding = RzActivityMyMeasurementBinding.inflate(layoutInflater)
        setContentView(am_binding.root)
        supportActionBar?.hide()

        RzAdsHandling.interstitialAd(this);

        RzAdsHandling.bannerAd(this,findViewById(R.id.ad_view))


        val am_recyclerView = am_binding.measureRecyclerview
        val am_adapter = RzMyAdapter(this,this)
        am_recyclerView.adapter = am_adapter
        am_recyclerView.layoutManager = LinearLayoutManager(this)

        datarecordViewModel = ViewModelProvider(this)[RzDataRecordViewModel::class.java]


        datarecordViewModel.allItems.observe(this, Observer { items ->
            items?.let { am_adapter.setItems(it) }
        })

        am_binding.searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                am_adapter.filter.filter(newText)
                return true
            }
        })

        am_binding.textView.setOnClickListener {
            RzAdsHandling.interstitialAd(this);
            val am_intent = Intent(this, RzMainActivity::class.java)
            startActivity(am_intent)
            finish()
        }

    }

}