package com.appcake.appruler.room.data

import androidx.lifecycle.LiveData

class RzDataRecordRepository(private val am_datarecordDao: RzDataRecordDao) {

    val amAllItems: LiveData<List<RzDataRecord>> = am_datarecordDao.getall()

    fun get(id: Long): LiveData<RzDataRecord> {
        return am_datarecordDao.get(id)
    }

    suspend fun update(item: RzDataRecord) {
        am_datarecordDao.update(item)
    }

    suspend fun insert(item: RzDataRecord) {
        am_datarecordDao.insert(item)
    }

    suspend fun delete(item: RzDataRecord) {
        am_datarecordDao.delete(item)
    }
}