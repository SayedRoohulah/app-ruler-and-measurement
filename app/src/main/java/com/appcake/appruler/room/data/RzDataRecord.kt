package com.appcake.appruler.room.data

import android.graphics.Bitmap
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.appcake.appruler.room.util.BitmapConverter


@Entity(tableName = "datarecords")
@TypeConverters(BitmapConverter::class)
data class RzDataRecord(
    @PrimaryKey(autoGenerate = true)
    val id: Long,
    val name: String,
    val image: Bitmap,
    val measure: String,
    val date: String
)
