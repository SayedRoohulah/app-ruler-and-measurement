package com.appcake.appruler.room.data

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface RzDataRecordDao {

    @Query("SELECT * from datarecords")
    fun getall(): LiveData<List<RzDataRecord>>

    @Insert(onConflict = OnConflictStrategy.ABORT)
    suspend fun insert(item: RzDataRecord)

    @Query("SELECT * FROM datarecords WHERE datarecords.id == :id")
    fun get(id: Long): LiveData<RzDataRecord>

    @Update
    suspend fun update(vararg items: RzDataRecord)

    @Delete
    suspend fun delete(vararg items: RzDataRecord)
}