package com.appcake.appruler.room.adapter

import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.appcake.appruler.R
import com.appcake.appruler.admob.RzAdsHandling
import com.appcake.appruler.room.activity.RzMyMeasurementActivity
import com.appcake.appruler.room.activity.RzRoomDetailActivity
import com.appcake.appruler.room.data.RzDataRecord
import java.io.ByteArrayOutputStream

class RzMyAdapter(private val am_activity: RzMyMeasurementActivity, private val am_context: Context) : RecyclerView.Adapter<RzMyAdapter.MyViewHolder>() ,
    Filterable {

    private var am_dataList = ArrayList<RzDataRecord>()

    override fun onCreateViewHolder(am_parent: ViewGroup, am_viewType: Int): MyViewHolder {
        val am_inflater = LayoutInflater.from(am_parent.context)
        val am_view = am_inflater.inflate(R.layout.rz_measurement_layout, am_parent, false)

        return MyViewHolder(am_view)
    }

    override fun onBindViewHolder(am_holder: MyViewHolder, am_position: Int) {
        val am_current = am_dataList[am_position]

        am_holder.am_itemName.text = am_current.name
        val am_bitmap: Bitmap = am_current.image
        am_holder.am_itemImage.setImageBitmap(am_bitmap)
        am_holder.am_itemMeasure.text = am_current.measure
        am_holder.am_itemDate.text = am_current.date

        if (am_position%3 == 0){
            RzAdsHandling.nativeAd(am_activity,am_holder.am_nativeAd,"small")
            am_holder.am_nativeAd.visibility = View.VISIBLE
         }
        // Set handlers
        am_holder.itemView.setOnClickListener{
            val am_intent = Intent(am_context, RzRoomDetailActivity::class.java)
            val am_stream = ByteArrayOutputStream()
            am_bitmap.compress(Bitmap.CompressFormat.PNG, 100, am_stream)
            val am_byteArray: ByteArray = am_stream.toByteArray()
           //intent.putExtra("name",am_current.name)
            am_intent.putExtra("id",am_current.id)
            am_intent.putExtra("image",am_byteArray)
            am_intent.putExtra("date",am_current.date)
            am_intent.putExtra("measure",am_current.measure)
            am_context.startActivity(am_intent)
        }
    }

    override fun getItemCount(): Int {
        return am_dataList.size
    }
    internal fun setItems(am_items: List<RzDataRecord>) {
        this.am_dataList =  am_items as ArrayList
        notifyDataSetChanged()
    }


    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val am_itemName: TextView = itemView.findViewById(R.id.item_name)
        val am_itemImage: ImageView = itemView.findViewById(R.id.item_image)
        val am_itemMeasure: TextView = itemView.findViewById(R.id.item_measure)
        val am_itemDate: TextView = itemView.findViewById(R.id.item_date)
        val am_nativeAd: FrameLayout = itemView.findViewById(R.id.nativeAd)
     }
    private var filteredList: List<RzDataRecord> = am_dataList
    private val filter = object : Filter() {
        override fun performFiltering(constraint: CharSequence?): FilterResults {
            val queryString = constraint?.toString()?.lowercase()
            val filterResults = FilterResults()
            filteredList = if (queryString == null || queryString.isEmpty()) {
                am_dataList
            } else {
                am_dataList.filter {
                    it.name.lowercase().contains(queryString) ||
                            it.measure.lowercase().contains(queryString)
                }
            }
            filterResults.values = filteredList
            return filterResults
        }

        override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
            filteredList = results?.values as List<RzDataRecord>
            notifyDataSetChanged()
        }

    }
    override fun getFilter(): Filter {
        return filter
    }

}