package com.appcake.appruler.room.viewmodel

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import com.appcake.appruler.room.data.RzAppRoomDatabase
import com.appcake.appruler.room.data.RzDataRecord
import com.appcake.appruler.room.data.RzDataRecordRepository
import kotlinx.coroutines.launch


private const val TAG = "DataRecordViewModel "

class RzDataRecordViewModel(application: Application) : AndroidViewModel(application) {

    private val am_repository: RzDataRecordRepository
    val allItems: LiveData<List<RzDataRecord>>

    init {
        Log.d(TAG, "Inside ViewModel init")
        val dao = RzAppRoomDatabase.getDatabase(application).rzDataRecordDao()
        am_repository = RzDataRecordRepository(dao)
        allItems = am_repository.amAllItems
    }

    fun insert(item: RzDataRecord) = viewModelScope.launch {
        am_repository.insert(item)
    }

    fun update(item: RzDataRecord) = viewModelScope.launch {
        am_repository.update(item)
    }

    fun delete(item: RzDataRecord) = viewModelScope.launch {
        am_repository.delete(item)
    }

    fun get(id: Long) = am_repository.get(id)
}