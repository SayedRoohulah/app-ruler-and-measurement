package com.appcake.appruler.room.activity

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.appcake.appruler.RzMainActivity
import com.appcake.appruler.R
import com.appcake.appruler.admob.RzAdsHandling
import com.appcake.appruler.databinding.RzActivityTipsandTrickBinding

class RzTipsandTrickActivity : AppCompatActivity() {
    private lateinit var am_binding: RzActivityTipsandTrickBinding

    override fun onCreate(am_savedInstanceState: Bundle?) {
        super.onCreate(am_savedInstanceState)
        am_binding = RzActivityTipsandTrickBinding.inflate(layoutInflater)
        setContentView(am_binding.root)

        supportActionBar?.hide()

        RzAdsHandling.interstitialAd(this)

        RzAdsHandling.bannerAd(this,findViewById(R.id.ad_view))
        am_binding.textView.setOnClickListener{
            RzAdsHandling.interstitialAd(this)
            val am_intent = Intent(this, RzMainActivity::class.java)
            startActivity(am_intent)
            finish()
        }

    }


}