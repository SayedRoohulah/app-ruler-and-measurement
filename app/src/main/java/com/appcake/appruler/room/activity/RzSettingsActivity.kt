package com.appcake.appruler.room.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.appcake.appruler.RzMainActivity
import com.appcake.appruler.R
import com.appcake.appruler.admob.RzAdsHandling
import com.appcake.appruler.databinding.RzActivitySettingsBinding

class RzSettingsActivity : AppCompatActivity() {
    lateinit var am_binding: RzActivitySettingsBinding

    override fun onCreate(am_savedInstanceState: Bundle?) {
        super.onCreate(am_savedInstanceState)
        am_binding = RzActivitySettingsBinding.inflate(layoutInflater)
        setContentView(am_binding.root)

        supportActionBar?.hide()

        RzAdsHandling.interstitialAd(this)

        RzAdsHandling.bannerAd(this,findViewById(R.id.ad_view))
        am_binding.tipsCard.setOnClickListener{
            val am_intent = Intent(this,RzTipsandTrickActivity::class.java)
            startActivity(am_intent)
        }

        am_binding.measurementCard.setOnClickListener{
            val am_intent = Intent(this,RzMyMeasurementActivity::class.java)
            startActivity(am_intent)
        }

        am_binding.textView.setOnClickListener {
            RzAdsHandling.interstitialAd(this)
            val am_intent = Intent(this,RzMainActivity::class.java)
            startActivity(am_intent)
            finish()
        }

    }



}