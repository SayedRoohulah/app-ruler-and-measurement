package com.appcake.appruler.room.activity

import android.app.AlertDialog
import android.app.Dialog
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.view.LayoutInflater
import android.view.Window
import android.widget.EditText
import android.widget.PopupMenu
import android.widget.TextView
 import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.appcake.appruler.R
import com.appcake.appruler.admob.RzAdsHandling
import com.appcake.appruler.databinding.RzActivityRoomDetailBinding
import com.appcake.appruler.room.data.RzDataRecord
import com.appcake.appruler.room.viewmodel.RzDataRecordViewModel


class RzRoomDetailActivity : AppCompatActivity() {
    private lateinit var binding: RzActivityRoomDetailBinding
    private lateinit var datarecordViewModel: RzDataRecordViewModel

    private lateinit var am_getDate: String
    private lateinit var am_getMeasure:String
    private lateinit var am_getImage: Bitmap
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = RzActivityRoomDetailBinding.inflate(layoutInflater)
        setContentView(binding.root)
        supportActionBar?.hide()

        RzAdsHandling.interstitialAd(this)
        RzAdsHandling.bannerAd(this,findViewById(R.id.ad_view))


        datarecordViewModel = ViewModelProvider(this)[RzDataRecordViewModel::class.java]


        val am_byteArray = intent.getByteArrayExtra("image")
        am_getImage = BitmapFactory.decodeByteArray(am_byteArray, 0, am_byteArray!!.size)
        binding.itemImage.setImageBitmap(am_getImage)

        //val name = intent.getStringExtra("name")
        am_getDate= intent.getStringExtra("date")!!
        am_getMeasure = intent.getStringExtra("measure")!!
      //  binding.itemName.text = name
        binding.itemDetail.text = am_getMeasure
        binding.itemDate.text = am_getDate

        binding.itemOption.setOnClickListener {
            amShowPopUp()
        }

        binding.textView.setOnClickListener {
            RzAdsHandling.interstitialAd(this)
            val am_intent = Intent(this, RzMyMeasurementActivity::class.java)
            startActivity(am_intent)
            finish()
        }

    }

    private fun amShowPopUp(){
        val am_popupMenu = PopupMenu(this, binding.itemOption)

        am_popupMenu.menuInflater.inflate(R.menu.rz_popup_menu, am_popupMenu.menu)
        am_popupMenu.setOnMenuItemClickListener(PopupMenu.OnMenuItemClickListener { menuItem -> // Toast message on menu item clicked
            when (menuItem.itemId) {
                R.id.rename->{
                    amShowEditDialog()
                }
                R.id.share -> {
                    //String s = mTextView.getText().toString();
                    val am_sharingIntent = Intent(Intent.ACTION_SEND)
                    am_sharingIntent.type = "text/plain"
                    am_sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "Subject Here")
                    am_sharingIntent.putExtra(
                        Intent.EXTRA_TEXT, ""
                    )
                    startActivity(Intent.createChooser(am_sharingIntent, "Share text via"))
                    return@OnMenuItemClickListener true
                }
                R.id.delete -> {
                  amShowDeleteDialog()
                }
                R.id.close ->{
                    am_popupMenu.dismiss()
                }
            }
            true
        })

        am_popupMenu.show()
    }
    private fun amShowDeleteDialog(){
        val am_dialog = Dialog(this)
        am_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        am_dialog.setCancelable(false)
        am_dialog.setContentView(R.layout.rz_custom_dialog)

        val am_title = am_dialog.findViewById(R.id.tvTitle) as TextView
        val am_yesBtn = am_dialog.findViewById(R.id.btn_Agree) as TextView
        am_yesBtn.text = "Yes"
        val am_noBtn = am_dialog.findViewById(R.id.btn_Cancel) as TextView
        am_noBtn.text = "NO"

        am_title.text = "Are you sure you want to delete this? Deleted measurment cannot be recovered."
        am_yesBtn.setOnClickListener {
            amDeleteRecord()
            am_dialog.dismiss()
        }
        am_noBtn.setOnClickListener {
            am_dialog.dismiss()
        }
        am_dialog.show()
    }
    private fun amDeleteRecord(){
        val getId = intent.getLongExtra("id",0)
        val item = RzDataRecord(id = getId, image = am_getImage, name = "", measure = am_getMeasure, date = am_getDate)
        datarecordViewModel.delete(item)
        finish()
    }

    fun amShowEditDialog() {
        val am_dialogView = LayoutInflater.from(this).inflate(R.layout.rz_edit_dialog, null)

        val am_editName = am_dialogView.findViewById<EditText>(R.id.editName)
        val am_editDes = am_dialogView.findViewById<EditText>(R.id.editDescription)

        val builder = AlertDialog.Builder(this)
        builder.setView(am_dialogView)
            .setTitle("Custom Dialog")
            .setPositiveButton("OK") { _, _ ->
                val amGetId = intent.getLongExtra("id",0)

                val amName = am_editName.text.toString()
                val amDes = am_editDes.text.toString()

                val amItem = RzDataRecord(id = amGetId, image = am_getImage, name = amName, measure = amDes, date = am_getDate)
                datarecordViewModel.update(amItem)
                finish()
            }
            .setNegativeButton("Cancel", null)

        val dialog = builder.create()
        dialog.show()
    }


}