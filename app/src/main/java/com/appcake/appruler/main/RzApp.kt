package com.appcake.appruler.main

import android.app.Activity
import android.os.Bundle
import androidx.multidex.MultiDexApplication
import com.adjust.sdk.Adjust
import com.adjust.sdk.AdjustConfig

class RzApp : MultiDexApplication() {
    override fun onCreate() {
        super.onCreate()


        ///////adjust sdk///////

        val appToken = "{123456}"

        val environment = AdjustConfig.ENVIRONMENT_SANDBOX

        val config = AdjustConfig(this, appToken, environment)

        Adjust.onCreate(config)



        registerActivityLifecycleCallbacks(AdjustLifecycleCallbacks())

    }



    class AdjustLifecycleCallbacks :  ActivityLifecycleCallbacks {

        override fun onActivityCreated(activity: Activity, savedInstanceState: Bundle?) {

        }

        override fun onActivityStarted(activity: Activity) {

        }

        override fun onActivityResumed(activity: Activity) {

            Adjust.onResume()

        }

        override fun onActivityPaused(activity: Activity) {

            Adjust.onPause()

        }

        override fun onActivityStopped(activity: Activity) {

        }

        override fun onActivitySaveInstanceState(activity: Activity, outState: Bundle) {
        }


        override fun onActivityDestroyed(activity: Activity) {

        }

    }
}