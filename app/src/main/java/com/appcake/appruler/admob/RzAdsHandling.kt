package com.appcake.appruler.admob
import android.app.Activity
import android.content.Context
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import com.appcake.appruler.R
import com.google.android.gms.ads.*
import com.google.android.gms.ads.initialization.InitializationStatus
import com.google.android.gms.ads.interstitial.InterstitialAd
import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback
import com.google.android.gms.ads.nativead.*
import java.util.*

class RzAdsHandling private constructor( ) {

    companion object {
        private var instance: RzAdsHandling? = null

        @JvmStatic
        fun getInstance(): RzAdsHandling {
            return instance ?: RzAdsHandling().also {
                instance = it
            }
        }
        private var mInterstitialAd: InterstitialAd? = null


    fun init(context: Context) {
        MobileAds.initialize(context) { initializationStatus: InitializationStatus ->
            val statusMap = initializationStatus.adapterStatusMap
            for ((adapterClass, status) in statusMap) {
                Log.d(
                    "MyApp", String.format(
                        "Adapter name: %s, Description: %s, Latency: %d",
                        adapterClass, status.description, status.latency
                    )
                )
            }
        }
        loadInterstitial(context)
    }

    fun bannerAd(context: Context, layout: FrameLayout) {
        val adView = AdView(context)
        adView.adUnitId = context.getString(R.string.banner_id)
        val adRequest = AdRequest.Builder().build()
        layout.addView(adView)
        adView.setAdSize(AdSize.BANNER)
        adView.loadAd(adRequest)
        adView.adListener = object : AdListener() {
            override fun onAdFailedToLoad(loadAdError: LoadAdError) {
                super.onAdFailedToLoad(loadAdError)
                layout.removeAllViews()
            }

            override fun onAdLoaded() {
                super.onAdLoaded()
                layout.removeAllViews()
                layout.addView(adView)
            }
        }
    }


    fun interstitialAd(context: Context) {
        if (mInterstitialAd != null) {
            mInterstitialAd!!.show(context as Activity)
        } else {
            loadInterstitial(context)
        }
    }

    fun loadInterstitial(context: Context) {
        val adRequest = AdRequest.Builder().build()
        InterstitialAd.load(context, context.getString(R.string.interstitial_id), adRequest,
            object : InterstitialAdLoadCallback() {
                override fun onAdLoaded(interstitialAd: InterstitialAd) {
                    super.onAdLoaded(interstitialAd)
                    mInterstitialAd = interstitialAd
                    interstitialAd.responseInfo.mediationAdapterClassName
                    interstitialAd.fullScreenContentCallback =
                        object : FullScreenContentCallback() {
                            override fun onAdFailedToShowFullScreenContent(adError: AdError) {
                                super.onAdFailedToShowFullScreenContent(adError)
                            }

                            override fun onAdShowedFullScreenContent() {
                                super.onAdShowedFullScreenContent()
                                if (mInterstitialAd == null) {
                                    loadInterstitial(context)
                                }
                            }

                            override fun onAdDismissedFullScreenContent() {
                                super.onAdDismissedFullScreenContent()
                                mInterstitialAd = null
                                loadInterstitial(context)
                            }
                        }
                }

                override fun onAdFailedToLoad(loadAdError: LoadAdError) {
                    super.onAdFailedToLoad(loadAdError)
                    mInterstitialAd = null
                }
            })
    }

    fun nativeAd(activity: Activity, frameLayout: FrameLayout, size: String) {
        val nativeId = activity.resources.getString(R.string.native_id)
        val builder = AdLoader.Builder(activity, nativeId)
        if (size == "small") {
            builder.forNativeAd { unifiedNativeAd: NativeAd ->
                val adView = activity.layoutInflater
                    .inflate(R.layout.rz_native_ad_view_small, null) as NativeAdView
                populateUnifiedNativeAdView(unifiedNativeAd, adView)
                frameLayout.addView(adView)
            }
        } else {
            builder.forNativeAd { unifiedNativeAd: NativeAd ->
                val adView = activity.layoutInflater
                    .inflate(R.layout.rz_native_ad_view_new, null) as NativeAdView
                populateUnifiedNativeAdView(unifiedNativeAd, adView)
                frameLayout.addView(adView)
            }
        }
        val adLoader = builder.withAdListener(object : AdListener() {}).build()
        adLoader.loadAd(AdRequest.Builder().build())
    }

    fun populateUnifiedNativeAdView(nativeAd: NativeAd, adView: NativeAdView) {
        // Set the media view.
        adView.mediaView = adView.findViewById(R.id.ad_media)

        // Set other ad assets.
        adView.headlineView = adView.findViewById(R.id.ad_headline)
        adView.bodyView = adView.findViewById(R.id.ad_body)
        adView.callToActionView = adView.findViewById(R.id.ad_call_to_action)
        adView.iconView = adView.findViewById(R.id.ad_app_icon)
        //        adView.setPriceView(adView.findViewById(R.id.ad_price));
//        adView.setStarRatingView(adView.findViewById(R.id.ad_stars));
//        adView.setStoreView(adView.findViewById(R.id.ad_store));
//        adView.setAdvertiserView(adView.findViewById(R.id.ad_advertiser));
        // The headline and mediaContent are guaranteed to be in every UnifiedNativeAd.
        (adView.headlineView as TextView?)!!.text = nativeAd.headline
        //adView.getMediaView().setMediaContent(nativeAd.getMediaContent());

        // These assets aren't guaranteed to be in every UnifiedNativeAd, so it's important to
        // check before trying to display them.
        if (nativeAd.body == null) {
            Objects.requireNonNull(adView.bodyView)?.visibility = View.INVISIBLE
        } else {
            adView.bodyView!!.visibility = View.VISIBLE
            (adView.bodyView as TextView?)!!.text = nativeAd.body
        }
        if (nativeAd.callToAction == null) {
            adView.callToActionView!!.visibility = View.INVISIBLE
        } else {
            adView.callToActionView!!.visibility = View.VISIBLE
            (adView.callToActionView as Button?)!!.text = nativeAd.callToAction
        }
        if (nativeAd.icon == null) {
            adView.iconView!!.visibility = View.GONE
        } else {
            (adView.iconView as ImageView?)!!.setImageDrawable(
                nativeAd.icon!!.drawable
            )
            adView.iconView!!.visibility = View.VISIBLE
        }

//        if (nativeAd.getPrice() == null) {
//            adView.getPriceView().setVisibility(View.INVISIBLE);
//        } else {
//            adView.getPriceView().setVisibility(View.VISIBLE);
//            ((TextView) adView.getPriceView()).setText(nativeAd.getPrice());
//        }
//
//        if (nativeAd.getStore() == null) {
//            adView.getStoreView().setVisibility(View.INVISIBLE);
//        } else {
//            adView.getStoreView().setVisibility(View.VISIBLE);
//            ((TextView) adView.getStoreView()).setText(nativeAd.getStore());
//        }

//        if (nativeAd.getStarRating() == null) {
//            adView.getStarRatingView().setVisibility(View.INVISIBLE);
//        } else {
//            ((RatingBar) adView.getStarRatingView())
//                    .setRating(nativeAd.getStarRating().floatValue());
//            adView.getStarRatingView().setVisibility(View.VISIBLE);
//        }

//        if (nativeAd.getAdvertiser() == null) {
//            adView.getAdvertiserView().setVisibility(View.INVISIBLE);
//        } else {
//            ((TextView) adView.getAdvertiserView()).setText(nativeAd.getAdvertiser());
//            adView.getAdvertiserView().setVisibility(View.VISIBLE);
//        }

        // This method tells the Google Mobile Ads SDK that you have finished populating your
        // native ad view with this native ad.
        adView.setNativeAd(nativeAd)
    }

    fun setNativeNull() {}
}
}